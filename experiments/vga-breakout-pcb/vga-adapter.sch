EESchema Schematic File Version 4
LIBS:vga-adapter-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R1
U 1 1 5B758B2B
P 5550 3350
F 0 "R1" V 5600 3500 50  0000 C CNN
F 1 "470R" V 5550 3350 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5480 3350 50  0001 C CNN
F 3 "~" H 5550 3350 50  0001 C CNN
	1    5550 3350
	0    1    1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 5B758B98
P 5550 3550
F 0 "R2" V 5600 3700 50  0000 C CNN
F 1 "470R" V 5550 3550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5480 3550 50  0001 C CNN
F 3 "~" H 5550 3550 50  0001 C CNN
	1    5550 3550
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 5B758BBE
P 5550 3750
F 0 "R3" V 5600 3900 50  0000 C CNN
F 1 "470R" V 5550 3750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5480 3750 50  0001 C CNN
F 3 "~" H 5550 3750 50  0001 C CNN
	1    5550 3750
	0    1    1    0   
$EndComp
Wire Wire Line
	5700 3350 6100 3350
$Comp
L Connector:DB15_Female_HighDensity_MountingHoles J1
U 1 1 5B75847C
P 6400 3750
F 0 "J1" H 6400 4617 50  0000 C CNN
F 1 "DB15_Female_HighDensity_MountingHoles" H 6400 4526 50  0000 C CNN
F 2 "Connector_Dsub:DSUB-15-HD_Female_Horizontal_P2.29x1.98mm_EdgePinOffset3.03mm_Housed_MountingHolesOffset4.94mm" H 5450 4150 50  0001 C CNN
F 3 " ~" H 5450 4150 50  0001 C CNN
	1    6400 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 3550 5700 3550
Wire Wire Line
	5700 3750 6100 3750
Wire Wire Line
	5400 3750 5150 3750
$Comp
L power:GND #PWR0101
U 1 1 5B758E79
P 5800 4500
F 0 "#PWR0101" H 5800 4250 50  0001 C CNN
F 1 "GND" H 5805 4327 50  0000 C CNN
F 2 "" H 5800 4500 50  0001 C CNN
F 3 "" H 5800 4500 50  0001 C CNN
	1    5800 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 4150 5800 4150
Wire Wire Line
	5800 4150 5800 4500
Wire Wire Line
	6100 3250 5800 3250
Wire Wire Line
	5800 3250 5800 3450
Connection ~ 5800 4150
Wire Wire Line
	6100 3450 5800 3450
Connection ~ 5800 3450
Wire Wire Line
	5800 3450 5800 3650
Wire Wire Line
	6100 3650 5800 3650
Connection ~ 5800 3650
Wire Wire Line
	5800 3650 5800 4050
Wire Wire Line
	6100 4050 5800 4050
Connection ~ 5800 4050
Wire Wire Line
	5800 4050 5800 4150
$Comp
L Device:R R4
U 1 1 5B759695
P 6850 4550
F 0 "R4" H 6700 4600 50  0000 L CNN
F 1 "68R" H 6650 4500 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6780 4550 50  0001 C CNN
F 3 "~" H 6850 4550 50  0001 C CNN
	1    6850 4550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5B75C177
P 6950 4150
F 0 "R5" H 6880 4104 50  0000 R CNN
F 1 "68R" H 6880 4195 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6880 4150 50  0001 C CNN
F 3 "~" H 6950 4150 50  0001 C CNN
	1    6950 4150
	-1   0    0    1   
$EndComp
Text Notes 7400 7500 0    50   ~ 0
dfgdfg
Wire Wire Line
	5400 3550 5250 3550
$Comp
L Connector:Conn_01x06_Male J2
U 1 1 5B7791AB
P 4050 3550
F 0 "J2" H 4156 3928 50  0000 C CNN
F 1 "Conn_01x06_Male" H 4156 3837 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 4050 3550 50  0001 C CNN
F 3 "~" H 4050 3550 50  0001 C CNN
	1    4050 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 3350 5400 3350
Wire Wire Line
	5250 3450 5250 3550
Wire Wire Line
	4250 3450 5250 3450
Wire Wire Line
	4250 3550 5150 3550
Wire Wire Line
	5150 3550 5150 3750
$Comp
L power:GND #PWR0102
U 1 1 5B779AC9
P 4350 3950
F 0 "#PWR0102" H 4350 3700 50  0001 C CNN
F 1 "GND" H 4355 3777 50  0000 C CNN
F 2 "" H 4350 3950 50  0001 C CNN
F 3 "" H 4350 3950 50  0001 C CNN
	1    4350 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 3850 4350 3850
Wire Wire Line
	4350 3850 4350 3950
Wire Wire Line
	6850 4800 5050 4800
Wire Wire Line
	5050 4800 5050 3650
Wire Wire Line
	5050 3650 4250 3650
Wire Wire Line
	4950 4900 4950 3750
Wire Wire Line
	4950 3750 4250 3750
Wire Wire Line
	4950 4900 6950 4900
Wire Wire Line
	6850 4700 6850 4800
Wire Wire Line
	6950 4300 6950 4900
Text Label 4600 3650 0    50   ~ 0
hsync
Text Label 4600 3750 0    50   ~ 0
vsync
Text Label 4600 3350 0    50   ~ 0
red
Text Label 4600 3450 0    50   ~ 0
green
Text Label 4600 3550 0    50   ~ 0
blue
Wire Wire Line
	6850 3750 6700 3750
Wire Wire Line
	6850 3750 6850 4400
Wire Wire Line
	6950 4000 6950 3950
Wire Wire Line
	6950 3950 6700 3950
$EndSCHEMATC
