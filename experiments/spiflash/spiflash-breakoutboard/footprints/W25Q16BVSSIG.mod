PCBNEW-LibModule-V1  
# encoding utf-8
Units mm
$INDEX
SOIC127P790X216-8N
$EndINDEX
$MODULE SOIC127P790X216-8N
Po 0 0 0 15 00000000 00000000 ~~
Li SOIC127P790X216-8N
Cd 
Sc 00000000
At SMD
Op 0 0 0
.SolderMask 0
.SolderPaste 0
T0 -1.69408 -3.51045 1.00241 1.00241 0 0.05 N V 21 "SOIC127P790X216-8N"
T1 -1.05729 3.50952 1.00214 1.00214 0 0.05 N V 21 "VAL**"
DC -5.085 -2.085 -4.985 -2.085 0.2 21
DC -2.175 -1.985 -2.075 -1.985 0.2 24
DS -2.69 -2.69 2.69 -2.69 0.127 24
DS -2.69 2.69 2.69 2.69 0.127 24
DS -2.69 -2.69 2.69 -2.69 0.127 21
DS -2.69 2.69 2.69 2.69 0.127 21
DS -2.69 -2.69 -2.69 2.69 0.127 24
DS 2.69 -2.69 2.69 2.69 0.127 24
DS -4.66 -2.94 4.66 -2.94 0.05 26
DS -4.66 2.94 4.66 2.94 0.05 26
DS -4.66 -2.94 -4.66 2.94 0.05 26
DS 4.66 -2.94 4.66 2.94 0.05 26
DS -2.69 -2.69 -2.69 -2.5 0.127 21
DS 2.69 -2.69 2.69 -2.5 0.127 21
DS 2.69 2.51 2.69 2.69 0.127 21
DS -2.69 2.51 -2.69 2.69 0.127 21
$PAD
Sh "1" R 1.61 0.58 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.605 -1.905
$EndPAD
$PAD
Sh "2" R 1.61 0.58 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.605 -0.635
$EndPAD
$PAD
Sh "3" R 1.61 0.58 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.605 0.635
$EndPAD
$PAD
Sh "4" R 1.61 0.58 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.605 1.905
$EndPAD
$PAD
Sh "5" R 1.61 0.58 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.605 1.905
$EndPAD
$PAD
Sh "6" R 1.61 0.58 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.605 0.635
$EndPAD
$PAD
Sh "7" R 1.61 0.58 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.605 -0.635
$EndPAD
$PAD
Sh "8" R 1.61 0.58 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.605 -1.905
$EndPAD
$EndMODULE SOIC127P790X216-8N
