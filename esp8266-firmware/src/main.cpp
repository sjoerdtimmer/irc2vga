#include <Arduino.h>
//#include <ESP8266WiFi.h>

#include <ESP8266WiFi.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>
#include <Time.h>

#include <WiFiUdp.h>
#include <NTPClient.h>

#include <Time.h>
#include <Timezone.h>

#include "ota.h"

int counter = 0;

const char * server = "openirc.snt.utwente.nl";
const int port = 6669;
const char * channel = "hsnijmegen";
const char * nick    = "marv";
char realnick[255];

WiFiClient client;

WiFiUDP ntpUDP;
NTPClient ntp(ntpUDP, "0.nl.pool.ntp.org", 0 /* 0 offset because we use Timezone lib for that*/, 60000);
TimeChangeRule CEST = {"CEST", Last, Sun, Mar, 2, 120};
TimeChangeRule CET  = {"CET",  Last, Sun, Oct, 3, 60};
Timezone tz(CEST, CET);

void setup()
{
    Serial.begin(9600);
    delay(10);
    Serial.println("\n\nWelcome to VGA-IRC-Display\n");
    Serial.println("Made by Themba");
    Serial.println("https://bitbucket.org/sjoerdtimmer/irc2vga");
    Serial.println("Based on the excellent Artekit VGA sources");
    Serial.println("www.artekit.eu/vga-output-using-a-36-pin-stm32/");
    Serial.println("License: GPL v2");
    

    WiFiManager wifiManager;
    wifiManager.autoConnect("VGA2IRC");

    Serial.println("wifi connected");

    setupOTA();
    ntp.begin();
    
    memcpy(realnick, nick, strlen(nick)+1); // one extra for 0 byte
    delay(1000);
}



#define IRC_BUFSIZE  32
char from[IRC_BUFSIZE];
char type[IRC_BUFSIZE];
char to[IRC_BUFSIZE];

void print_time();
void print_nick(char buffer[]);
int read_until(char abort_c, char buffer[]);
void ignore_until(char c);
void print_until(char c);
void print_until_endline();

void handle_irc_connection()
{
  char c;
  // if there are incoming bytes available 
  // from the server, read them and print them:
  while(true) {
      ArduinoOTA.handle();
      ntp.update();
      if (!client.connected()) {
          return;
      }
      if(client.available()) {
          c = client.read();
      }
      else {
          continue;
      }
      
      //Serial.print("char received: ");
      //Serial.println(c);
      if(c == ':') {
          memset(from, 0, sizeof(from));
          memset(type, 0, sizeof(type));
          memset(to, 0, sizeof(to));
          
          read_until(' ', from);
          read_until(' ', type);
          read_until(' ', to);
          
          //Serial.print("type: ");
          //Serial.println(type);
          //Serial.print("from: ");
          //Serial.println(from);
          //Serial.print("to: ");
          //Serial.println(to);
          
          
          if (strcmp(type, "PRIVMSG") == 0)
          {
              ignore_until(':');
              c = client.read(); // to determine whether this is a special PRIVMSG
              if (c == 0x01)
              {
//                  Serial.print("special priv:");
//                  print_until('\r');
//                  Serial.print("\n");
                  
                  int len = read_until(' ', type);
                  type[len-1] = 0;
                  if (strcmp(type, "ACTION") == 0)
                  {
                      Serial.print("[");
                      print_time();
                      Serial.print("]");
                      Serial.print("*");
                      print_nick(from);
                      Serial.print(" "); // original ' ' consumed by read_until
                      print_until(c); // the closing one
                      ignore_until('\r');
//                      Serial.print("\n");
                  }
                  else
                  {
                      //Serial.print("special privmsg:");
                      //Serial.print(type);
                      //print_until('\r');
                  }
              }
              else
              { // normal privmsg
                  Serial.print("[");
                  print_time();
                  Serial.print("]");
                  print_nick(from);
                  Serial.print(": \t");
                  Serial.print(c);
                  print_until('\r');
              }
          }
          else if (strcmp(type, "JOIN") == 0)
          {
              //Serial.println("JOIN:");
              //Serial.print("from: ");
              //Serial.println(from);
              //Serial.print("to: ");
              //Serial.println(to);
              //print_until('\r');
              for (int i = 0;i<IRC_BUFSIZE-1;i++)
              {
                  if (from[i]=='!')
                  {
                      from[i] = 0;
                      break;
                  }
              }
              //Serial.print("raw from:");
              //Serial.println(from);
              if (strcmp(from, realnick) == 0)
              {
                  Serial.print("[");
                  print_time();
                  Serial.print("]");
                  Serial.print("channel #");
                  Serial.print(channel);
                  Serial.println(" joined");
              }
              else
              {
                  Serial.print("[");
                  print_time();
                  Serial.print("]<");
                  Serial.print(from);
                  Serial.println("> joined");
              }
              
          }
          else if (strcmp(type, "TOPIC") == 0)
          {
              ignore_until(':');
              Serial.print("[");
              print_time();
              Serial.print("]");
              Serial.print("Topic set to \"");
              while(true)
              {
                  if (client.available())
                  {
                      char c = client.read();
                      if (c == '\r')
                          break;
                      Serial.print(c);
                  }
              }
              Serial.print("\" by ");
              print_nick(from);
              Serial.print("\n");
          }
          // TODO: do we need notice support?
          
          // else if() {
          // TODO: find out type of day-changed messages
          // }
          else if (strcmp(type, "433") == 0)
          {
              ignore_until('\r');
              int curlen = strlen(realnick);
              realnick[curlen] = '_';
              realnick[curlen+1] = 0;

              Serial.print("nick already in use, trying ");
              Serial.println(realnick);
              
              client.print("NICK ");
              client.print(realnick);
              client.print("\r\n");

              // we must join again
              client.print("JOIN #");
              client.print(channel);
              client.print("\r\n");
          }
          else
          {
              //Serial.print("ignored contents of command: ");
              //print_until('\r');
              ignore_until('\r');
          } 
      }
      // could be a PING request by the server.
      else if (c == 'P') {
          char buf[5];
          memset(buf, 0, sizeof(buf));
          buf[0] = c;
          for(int i = 1; i < 4; i++) {
              c = client.read();
              buf[i] = c;
          }
          ignore_until('\r');
//          Serial.print("ignored remainder or PING: ");
          //        print_until('\r');
          if(strcmp(buf, "PING") == 0) {
              client.print("PONG :");
              client.println(server);
              //Serial.println("PING->PONG");
          }
          else
          {
              //Serial.print("unindentified message 1: ");
              //Serial.print(buf);
              //print_until('\r');
          }
      }
      else
      {
          //Serial.println("some other message was received");
          //Serial.print(c);
          //print_until('\r');
      }
  } // end while
  
}

void print_nick(char buffer[])
{
    //  Serial.print("<");
    for(int i = 0; i < IRC_BUFSIZE - 1; i++) {
        if(buffer[i] == '!') { 
            break; 
        }    
        Serial.print(buffer[i]);
    }
    //Serial.print(">");
}

int read_until(char abort_c, char buffer[])
{
  int bytes_read = 0;
  memset(buffer, 0, sizeof(buffer));
  for(int i = 0; i < IRC_BUFSIZE - 1; i++) {
    if (client.available()) {
      char c = client.read();  
      bytes_read++;
      if(c == abort_c) {
        return bytes_read;
      }
      else if (c == '\n') {
        return bytes_read;
      }
      buffer[i] = c;
    }
  }
  ignore_until(abort_c);
  return bytes_read;
}

// reads characters from the connection until
// it hits the given character.
void ignore_until(char c)
{
  while(true){
    if (client.available()) {
      char curr_c = client.read();
      if (curr_c == c) {
        return;  
      }
    }
  }
}

// reads characters from the connection until
// it hits the given character.
void print_until(char c)
{
  while(true){
    if (client.available()) {
      char curr_c = client.read();
      if (curr_c == c) {
        Serial.println("");
        return;
      }
      Serial.print(curr_c);
    }
  }
}



// reads characters from the connection until
// it hits the given character.
void print_until_endline()
{
  while(true){
    if (client.available()) {
      char curr_c = client.read();
      if (curr_c == '\r') {
        curr_c = client.read();
        if (curr_c == '\n') { return; }
      }
      Serial.print(curr_c);
    }
  }
}


void print_time()
{
    TimeChangeRule * tcr;
    time_t local = tz.toLocal(ntp.getEpochTime(), &tcr);
    //Serial.print("raw time:");
    //Serial.println(ntp.getEpochTime());
    //Serial.print("local time:");
    //Serial.println(local);
    //setTime(local); // 
    int h = hour(local);
    if (h<10) Serial.print("0");
    Serial.print(h);
    Serial.print(":");
    int m = minute(local);
    if (m<10) Serial.print("0");
    Serial.print(m);
    Serial.print(":");
    int s = second(local);
    if (s<10) Serial.print("0");
    Serial.print(s);
    //Serial.print("(");
    //Serial.print(tcr->abbrev);
    //Serial.print(")");
}



void loop()
{
    ArduinoOTA.handle();
    ntp.update();
    if (!client.connected())
    {
        Serial.print("connecting to irc server ");
        Serial.print(server);
        Serial.print(":");
        Serial.println(port);

        if (client.connect(server, port))
        {
            Serial.println("connected");
            Serial.println("Requesting nick...");
            client.print("USER  marv 8 *  : irc2vga dongle\r\n");
            delay(500);
            client.print("NICK ");
            client.print(realnick);
            client.print("\r\n");
            delay(500);
            client.print("JOIN #");
            client.print(channel);
            client.print("\r\n");
            delay(500);
            
            handle_irc_connection();
        }
        else
        {
            Serial.println("connection failed");
            delay(5000);
        }
    }
    
    /*
    Serial.print("Hello:\tWorld ");
    Serial.print(++counter);
    if (counter % 15 == 0)
    {
        Serial.print(" is divisible by 15 so we print an extra long message that will not fit on one line...");
    }
    if (counter % 15 == 3)
    {
        Serial.print("\n message with a second line that has no tab");
    }
    if (counter % 15 == 6)
    {
        Serial.print("\n\textra message line that starts with a tab");
    }
    if (counter % 15 == 8)
    {
        Serial.print("\nHello world:\ttest with long text before tab");
    }
    
    Serial.print("\n");
    delay(500+random(2000));*/
}
