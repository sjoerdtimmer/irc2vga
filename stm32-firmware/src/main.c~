/***************************************************************************
 * STM32 VGA demo
 * Copyright (C) 2012 Artekit Italy
 * http://www.artekit.eu
 * Written by Ruben H. Meleca
 
### main.c
 
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   Gnu General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

***************************************************************************/

#include "stm32f10x.h"
#include "sys.h"
#include "video.h"
#include "gdi.h"
#include "stm32f10x_usart.h"
#include "stm32f10x_rcc.h"


extern void demoInit(void);

void RCC_Configuration(void)
{
	/* TIM1, GPIOA, GPIOB, GPIOE and AFIO clocks enable */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1 | RCC_APB2Periph_SPI1 | RCC_APB2Periph_TIM1 | RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_AFIO, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
}

void GPIO_Configuration(void)
{

}

void USART1_Init(void)
{
        /* USART configuration structure for USART1 */
    USART_InitTypeDef usart1_init_struct;
    /* Bit configuration structure for GPIOA PIN9 and PIN10 */
    GPIO_InitTypeDef gpioa_init_struct;
     
    /* Enalbe clock for USART1, AFIO and GPIOA */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1 | RCC_APB2Periph_AFIO | 
                           RCC_APB2Periph_GPIOA, ENABLE);
                            
    /* GPIOA PIN9 alternative function Tx */
    gpioa_init_struct.GPIO_Pin = GPIO_Pin_9;
    gpioa_init_struct.GPIO_Speed = GPIO_Speed_50MHz;
    gpioa_init_struct.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(GPIOA, &gpioa_init_struct);
    /* GPIOA PIN9 alternative function Rx */
    gpioa_init_struct.GPIO_Pin = GPIO_Pin_10;
    gpioa_init_struct.GPIO_Speed = GPIO_Speed_50MHz;
    gpioa_init_struct.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(GPIOA, &gpioa_init_struct);
 
    /* Enable USART1 */
    USART_Cmd(USART1, ENABLE);
    /* Baud rate 9600, 8-bit data, One stop bit
     * No parity, Do both Rx and Tx, No HW flow control
     */
    usart1_init_struct.USART_BaudRate = 9600;   
    usart1_init_struct.USART_WordLength = USART_WordLength_8b;  
    usart1_init_struct.USART_StopBits = USART_StopBits_1;   
    usart1_init_struct.USART_Parity = USART_Parity_No ;
    usart1_init_struct.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
    usart1_init_struct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    /* Configure USART1 */
    USART_Init(USART1, &usart1_init_struct);

    // update: no interrupt. that could screw with the video
    // instead I'll poll the buffer
    /* Enable RXNE interrupt */
    //USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
    /* Enable USART1 global interrupt */
    //NVIC_EnableIRQ(USART1_IRQn);
}



#define COLS 66
#define ROWS 25
#define TABCOL 12
//uint8_t screenDirty = 0;
char screenBuffer[ROWS][COLS+1]; // +1 to make sure there is always a terminating 0 byte

uint8_t cursor  = 0; // horizontal position
uint8_t curLine = 0; // line that is at the bottom of the scrolling buffer

uint32_t screenTicks, serialTicks;


void reprintScreen()
{
    vidClearScreen();
    for (int row=0; row<ROWS; row++)
    {
        gdiDrawTextEx(0, 8*row, screenBuffer[(row+curLine+1)%ROWS], GDI_ROP_COPY);
    }
    
    char txt[255];
    sprintf(txt, "curLine: %d", curLine);
    gdiDrawTextEx(0, 0, txt, GDI_ROP_COPY);
    sprintf(txt, "cursor: %d", cursor);
    gdiDrawTextEx(0, 8, txt, GDI_ROP_COPY);
}


void nextLine()
{
    curLine = (curLine + 1) % ROWS;
    memset(screenBuffer[curLine], 0, COLS); // clear line
    cursor = 0;
}

void gotoTab()
{
    while (cursor < TABCOL)
    {
        screenBuffer[curLine][cursor] = ' ';
        cursor++;
    }
}

void processChar(char newChar)
{
    if (newChar == '\n')
    {
        nextLine();
    }
    else if (newChar == '\t')
    {
        gotoTab();
    }
    else
    { // just an ordinary printable (presumable) char
        if (cursor==COLS)
        { // overflow!
            nextLine();
            gotoTab();
        }
        screenBuffer[curLine][cursor] = newChar;
        cursor++;
    }
//    screenDirty = 1;
    
//    reprintScreen();
}



void checkSerial()
{
    // check if receiver is not empty
    if (USART1->SR & USART_FLAG_RXNE)
    {
        //u32 start = sysMillis();
         int *STCSR = (int *)0xE000E010;
         int *STRVR = (int *)0xE000E014;
         int *STCVR = (int *)0xE000E018;

         int oldSTCSR = *STCSR;
         int oldSTRVR = *STRVR;
         int oldSTCVR = *STCVR;
         
         *STRVR = 0xFFFFFF;
         *STCVR = 0;
         *STCSR = 5;
         
         u32 start = *STCVR;
            
         while ( USART1->SR & USART_FLAG_RXNE )
         {
             processChar((char)(USART1->DR & 0xFF));
         }
//        reprintScreen();
//         serialTicks = sysMillis() - start;
         serialTicks = *STCVR - start;
         *STCSR = oldSTCSR;
         *STRVR = oldSTRVR;
         *STCVR = oldSTCVR;

    }
}


int main(void)
{
    serialTicks = 100;
 	RCC_Configuration();
	GPIO_Configuration();

        USART1_Init();
        
	vidInit();
	sysInitSystemTimer();
	
	//demoInit();
        for (int row=0; row<ROWS; row++)
        {
            memset(screenBuffer[row], 0, COLS+1);
//            screenBuffer[row][COLS] = 0;
        }

        while (1)
        {
//            if (screenDirty)
//            {
            uint32_t start = sysMillis();
            reprintScreen();
            screenTicks = sysMillis() - start;
            char txt[255];
            sprintf(txt, "screen update: %.2f ms", 0.01*screenTicks);
            gdiDrawTextEx(0,16, txt, GDI_ROP_COPY);
            sprintf(txt, "serial update: %.2f ms",  0.01*serialTicks);
            gdiDrawTextEx(0,24, txt, GDI_ROP_COPY);
//                screenDirty = 0;
//            }
//            vidClearScreen();
            sysDelayMs(500);
//            char txt[255];
//            sprintf(txt, "counter: %d", counter);
//            gdiDrawTextEx(180, 40, txt, GDI_ROP_COPY);

            //          sysDelayMs(1000);
        }
	return 0;
}


void USART1_IRQHandler(void)
{
    if (USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
    {
           processChar( (char)USART_ReceiveData(USART1));
    }
}
