/***************************************************************************
 * STM32 VGA demo
 * Copyright (C) 2012 Artekit Italy
 * http://www.artekit.eu
 * Written by Ruben H. Meleca
 
### main.c
 
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   Gnu General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

***************************************************************************/

#include "stm32f10x.h"
#include "sys.h"
#include "video.h"
#include "gdi.h"
#include "stm32f10x_usart.h"
#include "stm32f10x_rcc.h"

#define VTOTAL 52
extern volatile u16 vline;
extern __attribute__((aligned (4))) u8 fb[VID_VSIZE][VID_HSIZE+2];
extern const u8 gdiSystemFont[];


/* stuff for the circular serial buffer */
//#define BUFSIZE 1024;
//char uartbuffer[];


extern void demoInit(void);

void RCC_Configuration(void)
{
	/* TIM1, GPIOA, GPIOB, GPIOE and AFIO clocks enable */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1 | RCC_APB2Periph_SPI1 | RCC_APB2Periph_TIM1 | RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_AFIO, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
}


GPIO_InitTypeDef debugpin;
void GPIO_Configuration(void)
{
    RCC_APB1PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
    debugpin.GPIO_Pin = GPIO_Pin_1;
    debugpin.GPIO_Mode = GPIO_Mode_Out_PP;
    debugpin.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_Init(GPIOB, &debugpin);
    GPIO_ResetBits(GPIOB, GPIO_Pin_1);
}

void USART1_Init(void)
{
        /* USART configuration structure for USART1 */
    USART_InitTypeDef usart1_init_struct;
    /* Bit configuration structure for GPIOA PIN9 and PIN10 */
    GPIO_InitTypeDef gpioa_init_struct;
     
    /* Enalbe clock for USART1, AFIO and GPIOA */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1 | RCC_APB2Periph_AFIO | 
                           RCC_APB2Periph_GPIOA, ENABLE);
                            
    /* GPIOA PIN9 alternative function Tx */
    gpioa_init_struct.GPIO_Pin = GPIO_Pin_9;
    gpioa_init_struct.GPIO_Speed = GPIO_Speed_50MHz;
    gpioa_init_struct.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(GPIOA, &gpioa_init_struct);
    /* GPIOA PIN9 alternative function Rx */
    gpioa_init_struct.GPIO_Pin = GPIO_Pin_10;
    gpioa_init_struct.GPIO_Speed = GPIO_Speed_50MHz;
    gpioa_init_struct.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(GPIOA, &gpioa_init_struct);
 
    /* Enable USART1 */
    USART_Cmd(USART1, ENABLE);
    /* Baud rate 9600, 8-bit data, One stop bit
     * No parity, Do both Rx and Tx, No HW flow control
     */
    usart1_init_struct.USART_BaudRate = 9600;   
    usart1_init_struct.USART_WordLength = USART_WordLength_8b;  
    usart1_init_struct.USART_StopBits = USART_StopBits_1;   
    usart1_init_struct.USART_Parity = USART_Parity_No ;
    usart1_init_struct.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
    usart1_init_struct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    /* Configure USART1 */
    USART_Init(USART1, &usart1_init_struct);

    // update: no interrupt. that could screw with the video
    // instead I'll poll the buffer
    /* Enable RXNE interrupt */
    //USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
    /* Enable USART1 global interrupt */
    //NVIC_EnableIRQ(USART1_IRQn);
}



#define COLS 66
#define ROWS 23
#define TABCOL 20
//uint8_t screenDirty = 0;
char screenBuffer[ROWS][COLS+1]; // +1 to make sure there is always a terminating 0 byte

uint8_t cursor  = 0; // horizontal position
uint8_t curLine = 0; // line that is at the bottom of the scrolling buffer


void reprintScreen()
{
    vidClearScreen();
    //gdiRectangle(0,0,399,199, GDI_ROP_COPY);
    /*for (int row=0; row<ROWS; row++)
    {
        gdiDrawTextEx(2, 1+8*row, screenBuffer[(row+curLine+1)%ROWS], GDI_ROP_COPY);
    }*/
    

    GDI_WINDOW win = {
        GDI_WINCAPTION,
        {0,0,399,189},
        "IRCNet>#hsnijmegen>marvin_"
    };
    gdiDrawWindow(&win);
  
    
    //char txt[255];
    //sprintf(txt, "curLine: %d", curLine);
    //gdiDrawTextEx(0, 0, txt, GDI_ROP_COPY);
    //sprintf(txt, "cursor: %d", cursor);
    //gdiDrawTextEx(0, 8, txt, GDI_ROP_COPY);
}


void buildFrame()
{
    vidClearScreen();
    for (int i = 0; i < 11; i++) {
        gdiLine(NULL, 0, i, VID_HSIZE*8-1, i, GDI_ROP_COPY);
    }
    gdiLine(NULL, VID_HSIZE*8-1, 0, VID_HSIZE*8-1, VID_VSIZE-1, GDI_ROP_COPY);
    gdiLine(NULL, 0, 0, 0, VID_VSIZE-1, GDI_ROP_COPY);
    gdiLine(NULL, 0, VID_VSIZE-1, VID_HSIZE*8-1, VID_VSIZE-1, GDI_ROP_COPY);
    // make corners
    /*fb[0][0] &= ~(1<<7);
    fb[0][0] &= ~(1<<6);
    fb[1][0] &= ~(1<<7);

    fb[0][49] &= ~(1<<0 | 1<<1);
    //    fb[0][49] &= ~(1<<1);
    fb[1][49] &= ~(1<<0);*/
    gdiDrawTextEx(2, 1, "IRCNet>#hsnijmegen>", GDI_ROP_XOR);
}


void nextLine()
{
    curLine = (curLine + 1) % ROWS;
    memset(screenBuffer[curLine], ' ', COLS); // clear line
    cursor = 0;
}

void gotoTab()
{
    while (cursor < TABCOL)
    {
        screenBuffer[curLine][cursor] = ' ';
        cursor++;
    }
}

u8 newlinerequested = 0;
void processChar(char newChar)
{
    if (newlinerequested && newChar != '\r')
    {
        nextLine();
        newlinerequested = 0;
    }
    if (newChar == '\n')
    {
        newlinerequested = 1;
    }
    else if (newChar == '\t')
    {
        gotoTab();
    }
    else if (newChar == '\r')
    {
        cursor = 0;
    }
    else
    { // other chars
        if (newChar < ' ' || newChar > '~')
        {
            newChar = 0x7f; // the glyph to display unidentified chars
        }
        if (cursor==COLS)
        { // overflow!
            nextLine();
            gotoTab();
        }
        screenBuffer[curLine][cursor] = newChar;
        cursor++;
    }
//    screenDirty = 1;
    
//    reprintScreen();
}



// global counters to keep track of which part of the framebuffer needs to be refreshed next

u16 updphase = 0;
void checkSerial()
{
    //GPIO_SetBits(GPIOB, GPIO_Pin_1);
    // check incomming serial bytes:
    //if (USART1->SR & USART_FLAG_RXNE)
    //{
    while ( USART1->SR & USART_FLAG_RXNE )
    {
        processChar((char)(USART1->DR & 0xFF));
    }
        //reprintScreen();
        //}
    if (vline==0) // arbitrary value, but once per frame
    {
        updphase = (updphase + 1) % 3;
    }

    // refresh part of the frame buffer
    u16 line = (vline - 1 + VID_VSIZE) % VID_VSIZE; // line in screen (0-200)
    if (line >= 11 && line < VID_VSIZE-5)
    { // the first 11 and the last 5 lines are not text and should be left alone
        u8 txtline = (line - 11) / 8; // line in screenBuffer
        u8 glyphrow = (line - 11) % 8; // the row inside the glyph
        /* first attempt: this one is too slow
        for (u16 column = 3;column < VID_HSIZE/2-3; column++) // the first and the last 3 lines are part of the frame
        {
            u8 txtcol = (column - 3) / 6;
            u8 hoffs = (column - 3) % 6;
            u8 glyphbit = 7 - hoffs;
            u8 linebyte = column / 8;
            u8 linebit = column % 8;
            char c = screenBuffer[txtline][txtcol];
            if (gdiSystemFont[9*(c-GDI_SYSFONT_OFFSET)+voffs] & (1<<glyphbit))
            { //set bit
                fb[line][linebyte] |= 1 << (7 - linebit);
            }
            else
            { //clear bit
                fb[line][linebyte] &= ~ (1 << (7 - linebit));
            }
        }*/
        /* second attempt, refresh one byte at a time */
        
        for (u16 bytenum=updphase ; bytenum < 50 ; bytenum+=3) // only 37 bytes are needed since there are always 3 pixels blank and 1 line that is already there.
        {
            // every byte stretches over exactly 2 characters (chars are 5 pixels wide, there is 1 space left and 1 space right).
            // counting bits, the byte starts at bit (8*bytenum);
            // the first 3 bits of the line are padding.
            // after that a new char starts every 6 bits;
            // so the last bit of byte i belongs to character (8*i+7-3)/6;
            // (we reason from the back to avoid problem with the spaces surrounding chars)
            u8 charcol2 = (8 * bytenum + 4) / 6;
            u8 charcol1 = charcol2 - 1;
            // now get the chars from the screenBuffer
            char char1  = screenBuffer[(txtline+curLine+1)%ROWS][charcol1];
            char char2  = screenBuffer[(txtline+curLine+1)%ROWS][charcol2];
            if (charcol2 == 0) char1 = 0x20; // weird boundary case, pretend it is a space
            // now determine the byte from the glyph for these chars
            u8 glyphdata1 = gdiSystemFont[9*(char1-GDI_SYSFONT_OFFSET)+glyphrow];
            u8 glyphdata2 = gdiSystemFont[9*(char2-GDI_SYSFONT_OFFSET)+glyphrow];
            // determine the relative bit offsets of the glyphs
            u8 glyphoffset2 = 7 - (8 * bytenum + 4) % 6; // range is [2-7]
            //i16 glyphoffset1 = 6-glyphoffset2;
            // shift those bits:
            glyphdata2 = glyphdata2 >> glyphoffset2;
            if (glyphoffset2 < 6)
            {
                glyphdata1 <<= 6 - glyphoffset2;
            }
            else
            {
                glyphdata1 >>= glyphoffset2 - 6;
            }
            // write the byte to the framebuffer
            fb[line][bytenum] = glyphdata1 | glyphdata2;
        }
        
        fb[line][0] |= 1<<7; // left frame line
        fb[line][49] |= 1;
    }
    
    //GPIO_ResetBits(GPIOB, GPIO_Pin_1);
}


int main(void)
{
    RCC_Configuration();
    
    
    USART1_Init();
    GPIO_Configuration();
    
    vidInit();

    // sysTimer also generates noise
    //sysInitSystemTimer();
    
    //demoInit();
    for (int row=0; row<ROWS; row++)
    {
        memset(screenBuffer[row], ' ', COLS+1);
//            screenBuffer[row][COLS] = 0;
    }
    /*for (int y=0; y<ROWS; y++)
    {
        int len = sprintf(screenBuffer[y], "LA%d", y+1);
        screenBuffer[y][len] = ' ';
    }
    int len = sprintf(screenBuffer[5], "Hello world!!!!");
    screenBuffer[5][len] = ' '; */

    //reprintScreen();
    buildFrame();
    
    while (1)
    {
        // this needs to be empty or the screen will jitter
        // reprintScreen();
        //sysDelayMs(500);
        //GPIO_SetBits(GPIOB, GPIO_Pin_1);
        //sysDelayMs(500);
        //GPIO_ResetBits(GPIOB, GPIO_Pin_1);
        
    }
    return 0;
}

/*
void USART1_IRQHandler(void)
{

    if (USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
    {
        GPIO_SetBits(GPIOB, GPIO_Pin_1);
        char c = USART_ReceiveData(USART1);
        processChar(c);
        GPIO_ResetBits(GPIOB, GPIO_Pin_1);
    }
    

}*/
